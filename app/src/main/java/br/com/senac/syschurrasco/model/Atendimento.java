package br.com.senac.syschurrasco.model;



import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Atendimento implements Serializable {

    private static int id = 0 ;

    private int codigo  ;
    private NumberFormat nf = NumberFormat.getCurrencyInstance() ;
    private Date data  = new Date() ;
    private Cliente cliente  = new Cliente() ;
    private List<ItemAtendimento> itens  = new ArrayList<>() ;


    public Atendimento() {
        id ++ ;
        this.codigo = id ;
    }

    public Atendimento(Cliente cliente) {
        this() ;
        this.cliente = cliente;
    }

    public Atendimento(String nomeCliente){
        this() ;
        this.cliente.setNome(nomeCliente);
    }

    public Atendimento(Date data, Cliente cliente, List<ItemAtendimento> itens) {
        this() ;
        this.data = data;
        this.cliente = cliente;
        this.itens = itens;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemAtendimento> getItens() {
        return itens;
    }

    public void setItens(List<ItemAtendimento> itens) {
        this.itens = itens;
    }

    public void add(ItemAtendimento item) {
        this.itens.add(item) ;
    }

    public void remove(ItemAtendimento item){
        this.itens.remove(item) ;
    }


    public String getNomeCliente() {
        return this.cliente.getNome();
    }

    public String getTotalFormatado(){
        return nf.format(this.getTotal());
    }

    public double getTotal() {

       double total  = 0  ;

        for( ItemAtendimento item : this.itens ){
           total+= item.getTotal() ;
       }

        return total;
    }


    public int getCodigo() {
        return codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Atendimento that = (Atendimento) o;

        return codigo == that.codigo;

    }

    @Override
    public int hashCode() {
        return codigo;
    }
}
