package br.com.senac.syschurrasco.model;

import java.io.Serializable;

/**
 * Created by sala304b on 05/09/2017.
 */

public class Produto implements Serializable {

    private int codigo;
    private String nome;

    public Produto() {
    }

    public Produto(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
