package br.com.senac.syschurrasco.adapter;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.Atendimento;


public class AdapterAtendimento extends BaseAdapter {

    private List<Atendimento> lista  ;
    private Activity contexto ;

    public AdapterAtendimento(Activity contexto , List<Atendimento> lista){
        this.contexto = contexto ;
        this.lista = lista ;
    }



    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int posicao) {
        return this.lista.get(posicao);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {

        View view = contexto.getLayoutInflater()
                .inflate(R.layout.atendimento_lista , parent , false);

        TextView textViewNome = view.findViewById(R.id.lista_atendimento_nome);
        TextView textViewtotal = view.findViewById(R.id.lista_atendimento_total);

        Atendimento atendimento = this.lista.get(posicao) ;

        textViewNome.setText(atendimento.getNomeCliente());
        textViewtotal.setText(atendimento.getTotalFormatado());
        return view;
    }
}
