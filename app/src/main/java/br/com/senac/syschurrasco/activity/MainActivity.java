package br.com.senac.syschurrasco.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AlertDialogLayout;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.adapter.AdapterAtendimento;
import br.com.senac.syschurrasco.model.Atendimento;
import br.com.senac.syschurrasco.model.Cliente;
import br.com.senac.syschurrasco.model.ItemAtendimento;
import br.com.senac.syschurrasco.model.Produto;

public class MainActivity extends AppCompatActivity {

    public static final String ATENDIMENTO = "atendimento";
    public static final int NOVO = 1;
    public static final int EDITAR = 2;

    private List<Atendimento> listaAberto = new ArrayList<>();

    private List<Atendimento> listaFechado = new ArrayList<>();

    private List<Atendimento> listaCancelado = new ArrayList<>();

    private AdapterAtendimento adapterAtendimentoAberto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TabHost host = (TabHost) findViewById(R.id.tabHost);
        host.setup();

        //Tab1 - Abertos
        TabHost.TabSpec tabAbertos = host.newTabSpec("Abertos");
        tabAbertos.setContent(R.id.Abertos);
        tabAbertos.setIndicator("Abertos");
        host.addTab(tabAbertos);

        //Tab2 - Fechados
        TabHost.TabSpec tabFechados = host.newTabSpec("Fechados");
        tabFechados.setContent(R.id.Fechados);
        tabFechados.setIndicator("Fechados");
        host.addTab(tabFechados);

        //Tab3 - Cancelados
        TabHost.TabSpec tabCancelados = host.newTabSpec("Cancelados");
        tabCancelados.setContent(R.id.Cancelados);
        tabCancelados.setIndicator("Cancelados");
        host.addTab(tabCancelados);


        // soemnte para teste
        /*
        Atendimento atendimento = new Atendimento();
        atendimento.setCliente(new Cliente("Daniel"));
        atendimento.add(new ItemAtendimento(new Produto(1, "Churrasco"), 3, 3));
        listaAberto.add(atendimento);
        */

        adapterAtendimentoAberto = new AdapterAtendimento(this, listaAberto);

        ListView listViewAbertos = (ListView) findViewById(R.id.ListaAtendimentosAbertos);

        listViewAbertos.setAdapter(adapterAtendimentoAberto);

        listViewAbertos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int posicao, long id) {

                Atendimento atendimento = listaAberto.get(posicao);

                Intent intent = new Intent(MainActivity.this , AtendimentoActivity.class)  ;
                intent.putExtra(MainActivity.ATENDIMENTO, atendimento);
                startActivityForResult(intent, MainActivity.EDITAR);



            }
        });


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_atendimento_lista, menu);

        return true;
    }

    public void novo(MenuItem item) {

        //Criando dialog customizado para informar o nome do cliente
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nome do Cliente");

        final EditText input = new EditText(this);

        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String nome = input.getText().toString();
                novoAtendimento(nome);
            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });


        builder.show();


    }

    private void novoAtendimento(String nome) {
        Cliente cliente = new Cliente(nome);
        Atendimento atendimento = new Atendimento(cliente);

        Intent intent = new Intent(this, AtendimentoActivity.class);
        intent.putExtra(MainActivity.ATENDIMENTO, atendimento);
        startActivityForResult(intent, MainActivity.NOVO);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //pegar o objeto atendimento e colocar na lista  ...
        if (resultCode == RESULT_OK) {

            if (requestCode == MainActivity.NOVO) {

                Atendimento atendimento = (Atendimento) data.getSerializableExtra(MainActivity.ATENDIMENTO);
                listaAberto.add(atendimento);
                adapterAtendimentoAberto.notifyDataSetChanged();


            }else   if (requestCode == MainActivity.EDITAR) {
                //faz outra coisa ....
                Atendimento atendimento = (Atendimento) data.getSerializableExtra(MainActivity.ATENDIMENTO);
                int posicao =  listaAberto.indexOf(atendimento);
                listaAberto.set(posicao  , atendimento);

               /*
                for(int i = 0 ; i< listaAberto.size() ; i++){
                    if(listaAberto.get(i).equals(atendimento)){
                        listaAberto.set(i , atendimento) ;
                    }
                }
                */


                adapterAtendimentoAberto.notifyDataSetChanged();
            }
        }


    }


}
