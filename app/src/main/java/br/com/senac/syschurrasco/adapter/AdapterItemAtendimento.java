package br.com.senac.syschurrasco.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.senac.syschurrasco.R;
import br.com.senac.syschurrasco.model.ItemAtendimento;

public class AdapterItemAtendimento extends BaseAdapter{

    private List<ItemAtendimento> lista ;
    private Activity contexto ;

    public AdapterItemAtendimento(Activity contexto ,  List<ItemAtendimento> lista ){
        this.contexto = contexto;
        this.lista = lista ;
    }


    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int i) {
        return this.lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent) {

        View view = contexto.getLayoutInflater()
                .inflate(R.layout.item_atendimento , parent , false);

        ItemAtendimento item = this.lista.get(posicao);


        TextView textViewQuantidade = (TextView) view.findViewById(R.id.item_atendimento_quantidade_lista);
        TextView textViewPreco = (TextView) view.findViewById(R.id.item_atendimento_preco_lista);
        TextView textViewProduto = (TextView) view.findViewById(R.id.item_atendimento_produto_lista);
        TextView textViewTotal = (TextView) view.findViewById(R.id.item_atendimento_total_lista);

        textViewQuantidade.setText(String.format("%02d" , item.getQuantidade()));
        textViewPreco.setText(item.getPrecoFormatado());
        textViewProduto.setText(item.getProduto().getNome());
        textViewTotal.setText(item.getTotalFormatado());




        return view;
    }



















}
